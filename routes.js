var express = require('express')
  , router = express.Router()


router.use('/cars', require('./controllers/cars'))

router.use('/', require('./controllers/home'))


module.exports = router