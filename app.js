
var Twig = require("twig");
var express = require('express');


var app = express();
app.use('/static', express.static('public'));
app.use('/static', express.static('bower_components'));

// This section is optional and used to configure twig. 
app.set("twig options", {
    strict_variables: false
});


app.use(require('./routes'))

//run server
var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('App listening at http://'+ host+':'+port);
});